using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script : MonoBehaviour
{
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, 10 * Time.deltaTime);
        transform.GetChild(0).RotateAround(transform.position, Vector3.up, 30 * Time.deltaTime);
        transform.GetChild(0).GetChild(0).RotateAround(transform.GetChild(0).position, Vector3.up, 60 * Time.deltaTime);
    }

}
